import React from "react";
import { Component } from "react";

interface ButtonProps {
    text: string;
    onClick: any;
}

export default class ButtonAction extends Component<ButtonProps> {
    render() {
        return <>
            <div className="container">
                <button onClick={this.props.onClick}>{this.props.text}</button>
            </div>
        </>
    }
}