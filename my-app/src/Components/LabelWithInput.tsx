import React from "react";
import { Component } from "react";

interface LabelWithInputProps {
    labelText: string;
    inputType: string;
    onChange: any;
}
  
export default class LabelWithInput extends Component<LabelWithInputProps> {
    render() {
      return <>
        <div className="container">
            <div className="col-1-2">
                <label>{this.props.labelText}</label>
            </div>
            <div className="col-1-2">
                <input type={this.props.inputType} onChange={this.props.onChange}></input>
            </div>
        </div>
      </>
    }
}