import React, { Component } from "react";
import './Login.css';
import ButtonAction from '../Components/Button.tsx';
import LabelWithInput from '../Components/LabelWithInput.tsx';

interface LoginState {
    name: string;
    password: string;
    result: any;
}

export default class Login extends Component<{}, LoginState> {
    constructor(props:{})
    {
        super(props);
        this.state = { name: "", password: "", result: ""};
    }

    onClick = (e: any) => {
        fetch('/auth/login', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                username: this.state.name,
                password: this.state.password,
                // expiresInMins: 60, // optional
            })
        })
        .then(res => {
            if (res.status == 200)
            {
                this.setState({ result : "Succes"});
            }
            else
            {
                this.setState({ result : "Error"});
            }
            return res.json();
        } )
        .then(function (data) {
            console.log('Request succeeded with JSON response', data);
            })
        .catch(function (error) {
            console.log('Request failed', error);
        });
    }

    setPassword = (e: any) => {
        console.log(e.target.value);
        this.setState({ password: e.target.value });
    };
    
    setUsername = (e: any) => {
        console.log(e.target.value);
        this.setState({ name: e.target.value });
    };

    render () {
        return (
            <>
                username: 'kminchelle',
                password: '0lelplR',

                <LabelWithInput labelText="Login" inputType="text" onChange={this.setUsername} />
                <LabelWithInput labelText="Password" inputType="password" onChange={this.setPassword} />
                <ButtonAction text="Войти" onClick={this.onClick} />
                {this.state.result}
            </>);
    }
}